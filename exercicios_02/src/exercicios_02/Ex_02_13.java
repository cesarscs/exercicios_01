package exercicios_02;

import java.util.Scanner;

public class Ex_02_13 {

	private static Scanner teclado;

	public static void main(String[] args) {
		
//Declara��o de variaveis
	int num;

//Entrada de dados
	teclado = new Scanner(System.in);
	System.out.println("Digite um numero: ");
	num = teclado.nextInt();

//Processamento / Saida
	if (num %2 == 0)
		System.out.println("O numero � PAR");
		else
			System.out.println("O numero � IMPAR");
	}
}
