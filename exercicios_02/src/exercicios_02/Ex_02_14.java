package exercicios_02;

import java.util.Scanner;

public class Ex_02_14 {

	private static Scanner teclado;

	public static void main(String[] args) {
		
// Declaração de variaveis		
	int gols_gre, gols_int;

// Entrada de dados
	teclado = new Scanner(System.in);
	System.out.println("Total de gols do Gremio: ");
	gols_gre = teclado.nextInt();
	System.out.println("Total de gols do Inter: ");
	gols_int = teclado.nextInt();

//Processamento / Saida
	if (gols_gre > gols_int)
		System.out.println("Vencedor: Gremio");
		else if (gols_int > gols_gre)
			System.out.println("Vencedor: Inter");
			else
				System.out.println("Empate");
		}
}