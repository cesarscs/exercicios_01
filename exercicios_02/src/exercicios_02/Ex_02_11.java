package exercicios_02;

import java.util.Scanner;

public class Ex_02_11 {

	private static Scanner teclado;

	public static void main(String[] args) {

//Declaração de variaveis
	int senha;

//Entrada de dados
	teclado = new Scanner(System.in);
	System.out.println("Digite a senha: ");
	senha = teclado.nextInt();

//Processamento / Saida
	if (senha == 1234)
		System.out.println("ACESSO PERMITIDO");
		else
			System.out.println("ACESSO NEGADO");
	}		
}
