package exercicios_01;

import java.util.Scanner;

/**
 * 
 * @author Santana
 *
 */

//Fun��o:Calcular �rea do c�rculo
//Refazendo o exercicio 01 utilizando fun��o

public class Ex_01 {
	private static Scanner teclado;

//Fun��o
public static double circulo(final double Pi, double raio){
	return (Pi * raio * raio); // Formula para calculo
		}
public static void main(String[] args){
//Declara��o das Variaveis locais
	final double Pi = 3.14159;
		System.out.println("Digite o raio:");
		teclado = new Scanner(System.in);
		double raio = teclado.nextDouble();
			System.out.println("O valor do raio �: " + raio);
			System.out.println("O valor da �rea do c�rculo �: " + circulo(Pi,raio));
	}
}