package exercicios_01;

import java.util.Scanner;

public class Ex_04 {

	private static Scanner teclado;

	public static void main(String[] args) {
		
		// Vari�veis
		double pot_lamp, compr , larg, area, pot_tot, qtd_lamp;
		teclado = new Scanner(System.in);

		// Entrada
		System.out.println("Digite a potencia da lampada:");
		pot_lamp = teclado.nextDouble();
		System.out.println("Digite a comprimento:");
		compr = teclado.nextDouble();
		System.out.println("Digite a largura:");
		larg = teclado.nextDouble();
		
		// Formula calcular area Potencia total e quant lampadas
		area = compr * larg ;
		pot_tot = area * 18;
		qtd_lamp = (pot_tot / pot_lamp);
		// Resultado
		System.out.println("A quantidade de lampadas � : " + qtd_lamp);
	}

}

