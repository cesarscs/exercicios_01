package exercicios_01;

//Preencher (ler) um vetor X de 10 elementos com o valor inteiro 30.
//Escrever o vetor X ap�s seu total preenchimento.

public class Ex_01array {

	public static void main(String[] args) {
		
//Declara��o do vetor
			int[] vetArray = new int[10];

//exibindo o vetor
			System.out.println("Mostrando o vetor ");
			for(int i=0 ; i< vetArray.length ; i++){
				vetArray[i] = 30;
				System.out.println(" " + vetArray[i]);
			}
	}

}
