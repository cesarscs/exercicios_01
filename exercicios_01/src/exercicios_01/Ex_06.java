package exercicios_01;

import java.util.Scanner;

public class Ex_06 {

	private static Scanner teclado;

	public static void main(String[] args) {
		// Vari�veis
		double media, od_ini, od_fin, qtd_litr, vlr_tot, lucro;
		teclado = new Scanner(System.in);

		// Entrada
		System.out.println("Odometro inicial:");
		od_ini = teclado.nextDouble();
		System.out.println("Odometro final:");
		od_fin = teclado.nextDouble();
		System.out.println("Quantidade de litros:");
		qtd_litr = teclado.nextDouble();
		System.out.println("Valor total recebido:");
		vlr_tot = teclado.nextDouble();
		
		// Formula calcular media e lucro
		media = (od_fin - od_ini)/ qtd_litr ;
		lucro = vlr_tot - (qtd_litr * 1.90 );
		// Resultado
		System.out.println("A media do consumo � : " + media);
		System.out.println("O lucro � : " + lucro);

	}

}
