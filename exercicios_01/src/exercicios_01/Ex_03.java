package exercicios_01;

import java.util.Scanner;
/**
 * 
 * @author Santana
 *
 */

//Converter temperatura de Celsius para Fahrenheit

public class Ex_03 {

	private static Scanner teclado;

	public static void main(String[] args) {

		// Vari�veis
		double temp_f, temp_c;
		teclado = new Scanner(System.in);

		// Entrada
		System.out.println("Digite a temperatura em Celsius:");
		temp_c = teclado.nextDouble();

		// Formula Celsius para Fahrenheit
		temp_f = (temp_c * 9/5)+ 32 ;

		// Resultado
		System.out.println("A temperatura em Fahrenheit � : " + temp_f);
	}

}