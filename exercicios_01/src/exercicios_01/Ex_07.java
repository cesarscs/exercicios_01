package exercicios_01;

import java.util.Scanner;

public class Ex_07 {
	
	private static Scanner teclado;

	public static void main(String[] args) {

		//	A equipe Benneton-Ford deseja calcular o n�mero m�nimo de litros que dever� colocar no tanque
		//	de seu carro para que ele possa percorrer um determinado n�mero de voltas at� o primeiro
		//  reabastecimento. Escreva um programa que leia o comprimento da pista (em metros), o n�mero total
		//  de voltas a serem percorridas no grande pr�mio, o n�mero de reabastecimentos desejados e o
		//	consumo de combust�vel do carro (em Km/L). Calcular e escrever o n�mero m�nimo de litros
		//	necess�rios para percorrer at� o primeiro reabastecimento. OBS: Considere que o n�mero de voltas
		//	entre os reabastecimentos � o mesmo.
		//
		//  pista 10000 m
		//  voltas  10 
		//  reab     2
		//  consumo 1 km/l 
		//  cada abast  5 voltas
		//  percurso total  100000 m(100 km)
		//  percorr abast 10000 * 5 = 50000 m(50 km) 
		//  resultado 50 litros
		
		// Vari�veis
		double compr, t_volt, qtd_reab, cons, lit_min, volt_parad, perc_tot;
		teclado = new Scanner(System.in);

		// Entrada
		System.out.println("Digite o comprimento da pista(em metros):");
		compr = teclado.nextDouble();
		System.out.println("Total de voltas:");
		t_volt = teclado.nextDouble();
		System.out.println("Quantidade reabastecimentos:");
		qtd_reab = teclado.nextDouble();
		System.out.println("Consumo do carro Km/l:");
		cons = teclado.nextDouble();
		
		// Formula calcular
		perc_tot = (compr * t_volt)/1000; // percurso total em Km
		 volt_parad= (perc_tot/qtd_reab) ; // quant voltas por abast
		lit_min = volt_parad / cons  ; // quant minima litros
		// Resultado
		System.out.println("Numero minimo litros : " + lit_min);

	}

}
